import 'package:flutter/material.dart';
import 'Pages/HomeScreen.dart';
import 'Pages/EmailScreen.dart';
import 'Pages/PagesScreen.dart';
import 'Pages/AirPlayScreen.dart';

class TabBarWidget extends StatefulWidget {
  TabBarWidgetState createState() => TabBarWidgetState();
}

class TabBarWidgetState extends State<TabBarWidget> {
  // 定义属性
  final tintColor = Colors.blue;
  int currentIndex = 0;
  List<Widget> list = List();

  @override
  void initState() {
    // TODO: implement initState
    list..add(HomeScreen())
        ..add(EmailScreen())
        ..add(PagesScreen())
        ..add(AirPlayScreen());
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: list[currentIndex],
      bottomNavigationBar: BottomNavigationBar(items: [

        BottomNavigationBarItem(
          icon: Icon(
            Icons.home,
            color: tintColor,
          ),
          title: Text(
            "Home",
            style: TextStyle(color: tintColor),
          ),
        ),

        BottomNavigationBarItem(
          icon: Icon(
            Icons.email,
            color: tintColor,
          ),
          title: Text(
            "Email",
            style: TextStyle(color: tintColor),
          ),
        ),

        BottomNavigationBarItem(
          icon: Icon(
            Icons.pages,
            color: tintColor,
          ),
          title: Text(
            "Pages",
            style: TextStyle(color: tintColor),
          ),
        ),

        BottomNavigationBarItem(
          icon: Icon(
            Icons.airplay,
            color: tintColor,
          ),
          title: Text(
            "AirPlay",
            style: TextStyle(color: tintColor),
          ),
        ),
      ],

          currentIndex: currentIndex,
          onTap: (int index) {
            setState(() {
              currentIndex = index;
            });
          },
          type: BottomNavigationBarType.fixed,
      ),

    );
  }
}