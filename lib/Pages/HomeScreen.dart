import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:flutter_app_01/ProductModel.dart';

final screenWidth = window.physicalSize.width/window.devicePixelRatio;

class HomeScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar:AppBar(
          title: Text('HOME'),
          titleSpacing: 0,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                print("开始搜索");
                Navigator.push(context, MaterialPageRoute(builder: (context) => CustomSearchView()));
              },
            )
          ],
        ),
        body:Center(
          child: Text('HOME'),
        )
    );
  }
}

class CustomSearchView extends StatefulWidget {
  CustomSearchViewState createState() => CustomSearchViewState();
}

class CustomSearchViewState extends State<CustomSearchView> {
  List<ProductModel> items = [];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Container(
//                color: Colors.red,
                width: screenWidth-15-65,
                height: 33,
                margin: EdgeInsets.only(bottom: 10,left: 15),
                alignment: Alignment.center,

                child: Container(
                  padding: const EdgeInsets.only(left: 8),
                  alignment: Alignment.center,
                  decoration: new BoxDecoration(
                    color: Colors.white,
//                  border: new Border.all(color: Colors.black54, width: 4.0),
                    borderRadius: new BorderRadius.circular(5.0),
                  ),

                  child: TextField(
                      decoration: InputDecoration(
//                        filled: true,
//                        fillColor: Colors.red,
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.symmetric(vertical: 5),
                        hintText: "股票代码或简称",
                      ),
                    onChanged: (value) {
                        print(value);
                        ProductModel.queryBy(value).then((models) {
                          print(models);
                          setState(() {
                            items =  models;
                          });
                        });
                    },
                    autofocus: true,
                  ),
                ),
              ),
              Container(
                width: 65,
                height: 33,
                margin: EdgeInsets.only(bottom: 10),
                padding: EdgeInsets.only(right: 5,left: 5),
                alignment: Alignment.center,
//                color: Colors.red,

                child: FlatButton(
                    padding: EdgeInsets.all(0),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("取消",style: TextStyle(
                      fontSize: 18,
                      color: Colors.white
                    ),
                    softWrap: false,),
                ),
              )
            ],
          ),

        ],
      ),
      body: Container(
        child: ListView.builder(
          itemCount: items.length,
          itemBuilder: (context,index) {
            return Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      width: screenWidth - 50,
                      padding: EdgeInsets.only(left: 13),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(items[index].name,style: TextStyle(fontSize: 15,color: Color.fromRGBO(51, 51, 51, 1),fontFamily: "PingFangSC-Regular"),),
                          Text(items[index].code,style: TextStyle(fontSize: 12,color: Color.fromRGBO(127, 127, 127, 1),fontFamily: "PingFangSC-Regular"),),
                        ],
                      ),
                    ),
                    IconButton(icon: Icon(Icons.add), onPressed: () {

                    })
                  ],
                ),
                new Divider(height: 0.5,),
              ],
            );
          },
          itemExtent: 53,
        ),
      ),
    );
  }
}
