import 'package:flutter_orm_plugin/flutter_orm_plugin.dart';

class ProductModel {
  String code;
  String name;
  String firstChar;
  int type;
  String mainFlag;

  ProductModel.make(this.code,this.name,this.firstChar,this.type,this.mainFlag);
  String toString() => '$code $name $firstChar $type $mainFlag';

  static String get dbName => "models.db";
  static String get tableName => "productModels";

  static createTable() {
    Map<String , Field> fields = new Map<String , Field>();
    fields["code"] = Field(FieldType.Text, primaryKey: true);
    fields["name"] = Field(FieldType.Text);
    fields["type"] = Field(FieldType.Integer);
    fields["firstChar"] = Field(FieldType.Text, foreignKey: true, to: "first_char");
    fields["mainFlag"] = Field(FieldType.Text, foreignKey: true, to: "main_flag");
    FlutterOrmPlugin.createTable(dbName,tableName,fields);
  }

  static Future<void> insertModels(List<ProductModel> models) async {

    createTable();

    List orms = new List();
    for(int i = 0 ; i < models.length ; i++) {
      Map m = {"code":models[i].code, "name":models[i].name, "type":models[i].type, "firstChar":models[i].firstChar,"mainFlag":models[i].mainFlag};
      orms.add(m);
    }
    await FlutterOrmPlugin.batchSaveOrms(tableName, orms);
  }



  static  Future<List<ProductModel>> queryAll() async {

    createTable();// 没有db 没有table 直接Query是会闪退的

    List<ProductModel> models = [];
    final values =  await Query(tableName).all();

    for (int i = 0 ; i < values.length ; i++) {
      Map value = values[i];
      models.add(ProductModel.make(value["code"], value["name"], value["firstChar"], (value["type"] as double).toInt(), value["mainFlag"]));
    }
    return models;
  }

  static Future<List<ProductModel>> queryBy(String str) async {
    if (str == null || str.length <= 0) {
      return [];
    }
    var sql = "type = 2 and (code like '%$str%' or name like '%$str%' or firstChar like '%$str%') ORDER BY code ASC";
    final values =  await Query(tableName)
                          .whereBySql(sql, [])
                          .limit(20)
                          .all();

    if (values == null) {
      return [];
    }

    List<ProductModel> models = [];


    for (int i = 0 ; i < values.length ; i++) {
      Map value = values[i];
      models.add(ProductModel.make(value["code"], value["name"], value["firstChar"], (value["type"] as double).toInt(), value["mainFlag"]));
    }
    return models;
  }

}