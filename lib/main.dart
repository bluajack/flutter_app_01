import 'dart:convert';
import 'dart:isolate';
import 'package:flutter/material.dart';
import 'TabBarWidget.dart';
import 'package:flutter_app_01/ProductModel.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  initProductModel();
  runApp(new MyApp());
}



// 初始化代码数据包
initProductModel() async {
  try {
//    final wq = await getApplicationDocumentsDirectory();
//    print(wq.path);

    Response response = await Dio().cget("http://fc.xishisoft.com/api/api/product_package");
    final jsonData = json.decode(response.data.toString());
    final data = jsonData["data"];
    final url = data["url"];
    if (url != null) {// 数据包下载地址
      Response downloadResponse = await Dio().cget(url);
      List<dynamic> values =  downloadResponse.data;
      print(values.length);
      List<ProductModel> models = new List();
      values.forEach((value) {
        var mainFlag = value["main_flag"];
        if (mainFlag.runtimeType == int) {
          mainFlag = (mainFlag as int).toString();
        }
        ProductModel model = ProductModel.make(value["code"], value["name"], value["first_char"], value["type"], mainFlag);
        models.add(model);
      });
      await ProductModel.insertModels(models);
    }
  } on DioError catch (e) {

  }
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title:'Flutter bottomNavigationBar',
        theme:ThemeData.light(),
        home: TabBarWidget(),
    );
  }
}